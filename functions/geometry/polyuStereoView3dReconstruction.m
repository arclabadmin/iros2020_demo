function [mp0, mp1, point3D0, point3D1] = polyuStereoView3dReconstruction(...
    img0, img1, cameraParams0, cameraParams1, T_c0c1, T_c1c0,...
    featurePara, sos_para, minimumPoint)
% stereo vision
% tic;
Points0 = polyuDetectFeature(img0, featurePara);
Points1 = polyuDetectFeature(img1, featurePara);
% Points0 = polyuDetectOrbFeature(img0, featurePara);
% Points1 = polyuDetectOrbFeature(img1, featurePara);
% fd_time_cost = toc
% view 0 point tracking
[Points0_tracked, validIdx0] = polyuPointTracker(Points0, img0, img1, minimumPoint, featurePara);
outliers0 = polyuRemoveLargeShift(Points0, Points0_tracked, validIdx0);
validIdx0(outliers0) = 0;
mp0 = Points0(validIdx0,:);
mp0_tracked = Points0_tracked(validIdx0,:);
points3D_cam0 = polyu3Dreconstruct(T_c0c1, mp0, mp0_tracked, cameraParams0, cameraParams1);
points3D_cam0 = points3D_cam0';
% filter bad 3D points
outliers_negdepth = find(points3D_cam0(3,:) < 0);
outliers_farpoint = polyuFindFarPoints(points3D_cam0);
outliers = [outliers_negdepth'; outliers_farpoint];
points3D_cam0(:,outliers) = [];
mp0(outliers,:) = [];
mp0_tracked(outliers,:) = [];
% view 1 point tracking
[Points1_tracked, validIdx1] = polyuPointTracker(Points1, img1, img0, minimumPoint, featurePara);
outliers1 = polyuRemoveLargeShift(Points1, Points1_tracked, validIdx1);
validIdx1(outliers1) = 0;
mp1 = Points1(validIdx1,:);
mp1_tracked = Points1_tracked(validIdx1,:);
points3D_cam1 = polyu3Dreconstruct(T_c1c0, mp1, mp1_tracked, cameraParams1, cameraParams0);
points3D_cam1 = points3D_cam1';
% filter bad 3D points
outliers_negdepth = find(points3D_cam0(3,:) < 0);
outliers_farpoint = polyuFindFarPoints(points3D_cam1);
outliers = [outliers_negdepth'; outliers_farpoint];
points3D_cam1(:,outliers) = [];
mp1(outliers,:) = [];
mp1_tracked(outliers,:) = [];
% convert view 1 to view 0
R = T_c0c1(1:3,1:3);
t = T_c0c1(1:3,4);
try
    points3D_c1c0 = R * points3D_cam1 + repmat(t,1,length(points3D_cam1));
catch
    points3D_c1c0 = [];
end
% convert view 0 to view 1
R = T_c1c0(1:3,1:3);
t = T_c1c0(1:3,4);
try
    points3D_c0c1 = R * points3D_cam0 + repmat(t,1,length(points3D_cam0));
catch
    points3D_c0c1 = [];
end
% merge two views
mp0 = [mp0; mp1_tracked];
mp1 = [mp0_tracked; mp1];
% figure(1);
% showMatchedFeatures(img0, img1, mp0, mp1);

point3D0 = [points3D_cam0'; points3D_c1c0'];
point3D1 = [points3D_c0c1'; points3D_cam1'];
% mp1 = mp0_tracked;
% point3D0 = points3D_cam0';
% point3D1 = points3D_c0c1';
% sum of square check
if sos_para(3)
    orient_gt = T_c0c1(1:3,1:3)';
    loc_gt = T_c0c1(1:3,4)';
    [E, inlierIdx] = estimateEssentialMatrix(...
        mp0, mp1, cameraParams0, cameraParams1, 'Confidence', 90);
    if sum(inlierIdx)/length(inlierIdx) < 0.5
        inlier_refined = polyuSumOfSquareRefinement(mp1', point3D0', orient_gt, loc_gt, cameraParams1, sos_para);
        if length(inlier_refined) > minimumPoint
            mp0 = mp0(inlier_refined,:);
            mp1 = mp1(inlier_refined,:);
            point3D0 = point3D0(inlier_refined,:);
            point3D1 = point3D1(inlier_refined,:);
        end
    end
end

% figure(2);
% subplot(1,2,1);
% hold on;
% plot3(points3D_cam0(1,:),points3D_cam0(2,:),points3D_cam0(3,:),'b.');
% plot3(points3D_c1c0(1,:),points3D_c1c0(2,:),points3D_c1c0(3,:),'r.');
% hold off;
% xlabel('view 0');
% subplot(1,2,2);
% hold on;
% plot3(points3D_cam1(1,:),points3D_cam1(2,:),points3D_cam1(3,:),'b.');
% plot3(points3D_c0c1(1,:),points3D_c0c1(2,:),points3D_c0c1(3,:),'r.');
% hold off;
% xlabel('view 1');

return

% % if minEigen feature failed, try ORB feature
% if size(mp0,1) < 20
%     % stereo vision
%     points = detectORBFeaturesOCV(img0);
%     Points0 = points.Location;
%     points = detectORBFeaturesOCV(img1);
%     Points1 = points.Location;
%     [Points0_tracked, validIdx0] = polyuPointTracker(Points0, img0, img1);
%     [Points1_tracked, validIdx1] = polyuPointTracker(Points1, img1, img0);
%     if sum(validIdx0) > 20
%         mp0 = Points0(validIdx0,:);
%         mp0_tracked = Points0_tracked(validIdx0,:);
%     else
%         mp0 = Points0;
%         mp0_tracked = Points0_tracked;
%     end
%     if sum(validIdx1) > 20
%         mp1 = Points1(validIdx1,:);
%         mp1_tracked = Points1_tracked(validIdx1,:);
%     else
%         mp1 = Points1;
%         mp1_tracked = Points1_tracked;
%     end
%     points3D_cam0 = polyu3Dreconstruct(T_c0c1, mp0, mp0_tracked, cameraParams0, cameraParams1);
%     points3D_cam0 = points3D_cam0';
%     points3D_cam1 = polyu3Dreconstruct(T_c1c0, mp1, mp1_tracked, cameraParams1, cameraParams0);
%     points3D_cam1 = points3D_cam1';
%     % convert view 1 to view 0
%     R = T_c0c1(1:3,1:3);
%     t = T_c0c1(1:3,4);
%     try
%         points3D_c1c0 = R * points3D_cam1 + repmat(t,1,length(points3D_cam1));
%     catch
%         points3D_c1c0 = [];
%     end
%     % convert view 0 to view 1
%     R = T_c1c0(1:3,1:3);
%     t = T_c1c0(1:3,4);
%     try
%         points3D_c0c1 = R * points3D_cam0 + repmat(t,1,length(points3D_cam0));
%     catch
%         points3D_c0c1 = [];
%     end
%     % merge two views
%     mp0 = [mp0; mp1_tracked];
%     mp1 = [mp0_tracked; mp1];
%     point3D0 = [points3D_cam0'; points3D_c1c0'];
%     point3D1 = [points3D_c0c1'; points3D_cam1'];
% end