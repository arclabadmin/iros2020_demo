function [scale, outliers, indices] = polyuGetDepthScale(old_depth, new_depth, valid_range)
if nargin < 3
    valid_range = 10;
end
if size(old_depth,1) < size(new_depth,1)
    indices = dsearchn(new_depth(:,1:2), old_depth(:,1:2));
    dist = old_depth(:,1:2) - new_depth(indices,1:2);
    dist_norm = vecnorm(dist');
    outliers = find(dist_norm) > valid_range;
    scale = old_depth(:,3) ./ new_depth(indices,3);
else
    indices = dsearchn(old_depth(:,1:2), new_depth(:,1:2));
    dist = old_depth(indices,1:2) - new_depth(:,1:2);
    dist_norm = vecnorm(dist');
    outliers = find(dist_norm) > valid_range;
    scale = old_depth(indices,3) ./ new_depth(:,3);
end

return