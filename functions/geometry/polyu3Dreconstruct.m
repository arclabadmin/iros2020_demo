% =====================================================================

% =====================================================================
function points3D = polyu3Dreconstruct(T, inlierPoints0, inlierPoints1, cameraParams0, cameraParams1)
T = T';
orient = T(1:3,1:3);
loc = T(4,1:3);
[Re, te] = cameraPoseToExtrinsics(orient, loc);
camMatrix0 = cameraMatrix(cameraParams0, eye(3), [0 0 0]);
camMatrix1 = cameraMatrix(cameraParams1, Re, te);
points3D = triangulate(inlierPoints0, inlierPoints1, camMatrix0, camMatrix1);
% points3D = points3D';
return