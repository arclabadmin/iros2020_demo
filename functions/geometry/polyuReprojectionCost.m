function error_vector = polyuReprojectionCost(point2D, point3D, orient, loc)
    v = cayley_R2v(orient);
    skew_v = skew(v);
    x0 = [v;(eye(3) - skew_v)*loc'];
    nPoints = size(point2D,2);
    for ii = 1:nPoints
        p1 = point3D(:,ii);
        p2 = point2D(:,ii);
        Q1 = compute_Q1(p1(1),p1(2),p1(3),p2(1),p2(2));
        Q2 = compute_Q1(p1(1),p1(2),p1(3),p2(1),p2(2));
        Q3 = compute_Q3(p1(1),p1(2),p1(3),p2(1),p2(2));
        error_vector(ii,1) = norm([[x0;1]'*Q1*[x0;1], [x0;1]'*Q2*[x0;1], [x0;1]'*Q3*[x0;1]]);
    end
    error_vector = sort(error_vector,'ascend');
end

%     P = [1,0,0,0;
%         0,1,0,0;
%         0,0,1,0];
%     check = P*[orient,loc'; 0 0 0 1]*point3D;
%     check = check./repmat(check(3,:),3,1);
%     error_vector = sum(abs(check - point2D));