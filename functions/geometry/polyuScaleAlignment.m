function [mean_scale, outliers] = polyuScaleAlignment(scale)

N = length(scale);
scale_filtered = medfilt1(scale);
phat = mle(scale_filtered, 'distribution', 'norm');
mean_scale = phat(1);
scale_var = phat(2);
error = abs(scale - mean_scale * ones(N,1));
% outliers = find(error > mean_scale + 2*scale_var);
% outliers = find(error > 2*scale_var);
outliers = find(error > max(0.05, 2*scale_var));

return