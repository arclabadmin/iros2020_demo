function errors = checkReprojectionError(point2D, point3D, orient_r2f, loc_r2f)

    v = cayley_R2v(orient_r2f);
    t = (eye(3) - skew(v)) * (-loc_r2f*orient_r2f')';
    x = [v;t;1];
    
    N = size(point2D,2);
    errors = zeros(3*N,1);
    for ii = 1 : N
        p1 = point3D(:,ii);
        p2 = point2D(:,ii);
        Q1 = compute_Q1(p1(1),p1(2),p1(3),p2(1),p2(2));
        Q2 = compute_Q2(p1(1),p1(2),p1(3),p2(1),p2(2));
        Q3 = compute_Q3(p1(1),p1(2),p1(3),p2(1),p2(2));
        errors((ii-1)*3 + 1) = x'*Q1*x;
        errors((ii-1)*3 + 2) = x'*Q2*x;
        errors((ii-1)*3 + 3) = x'*Q3*x;
    end

return