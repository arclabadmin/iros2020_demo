function v = cayley_R2v(R)
    I = eye(3);
    skew_v = (R - I)*pinv(R + I);
    v = [skew_v(3,2); skew_v(1,3); skew_v(2,1)];
return