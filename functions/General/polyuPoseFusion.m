function T_uav = polyuPoseFusion(T_uav0, T_uav1, outlier_percent0, outlier_percent1)
T_uav = eye(4);
if outlier_percent0 < 0.5 && outlier_percent1 > 0.5
    T_uav = T_uav0;
elseif outlier_percent0 > 0.5 && outlier_percent1 < 0.5
    T_uav = T_uav1;
else
    v0 = cayley_R2v(T_uav0(1:3,1:3));
    v1 = cayley_R2v(T_uav1(1:3,1:3));
    s = outlier_percent0 + outlier_percent1;
    if s == 0
        v = (v0 + v1).*0.5;
        t = (T_uav0(1:3,4) + T_uav1(1:3,4)).*0.5;
        T_uav(1:3,1:3) = cayley_v2R(v);
        T_uav(1:3,4) = t;
    else
        w0 = (s - outlier_percent0)/s;
        w1 = 1 - w0;
        v = w0 * v0 + w1 * v1;
        t = w0 * T_uav0(1:3,4) + w1 * T_uav1(1:3,4);
        T_uav(1:3,1:3) = cayley_v2R(v);
        T_uav(1:3,4) = t;
    end
end
return