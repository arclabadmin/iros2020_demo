function R = cayley_v2R(v)
    I = eye(3);
    skew_v = skew(v);
    R = pinv(I - skew_v) * (I + skew_v);
return