function [connection, outliers] = polyuFindConnection(pointKeyFrame, pointReproject, noiseLevel)

[connection, d] = dsearchn(pointReproject, pointKeyFrame);
outliers = find(d > noiseLevel);

return