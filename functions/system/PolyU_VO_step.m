function polyuVO = PolyU_VO_step(polyuVO, currentFrame0, currentFrame1)
disp('-----------------new frame--------------------');
% load data
lastFrame0 = polyuVO.lastFrame0;
lastFrame1 = polyuVO.lastFrame1;
cameraParams0 = polyuVO.cameraParams0;
cameraParams1 = polyuVO.cameraParams1;
K0 = cameraParams0.IntrinsicMatrix';
K1 = cameraParams1.IntrinsicMatrix';
UAV_pose_keyframe = polyuVO.keyframe{polyuVO.keyframeID}.pose;
% load camera info and previous transform
T_cam2body0 = polyuVO.T_cam2body0;
T_cam2body1 = polyuVO.T_cam2body1;
T_body2cam0 = polyuVO.T_body2cam0;
T_body2cam1 = polyuVO.T_body2cam1;
pre_T = polyuVO.pre_T;
pre_T_cam0 = T_cam2body0 * pre_T * T_body2cam0;
pre_T_cam1 = T_cam2body1 * pre_T * T_body2cam1;
pre_orient_r2f0 = pre_T_cam0(1:3,1:3)';
pre_orient_r2f1 = pre_T_cam1(1:3,1:3)';
pre_loc_r2f0 = pre_T_cam0(1:3,4)';
pre_loc_r2f1 = pre_T_cam1(1:3,4)';
% points tracking
Points0 = polyuVO.tracks0;
Points1 = polyuVO.tracks1;
point3D0 = polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam0;
point3D1 = polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam1;
[Points0_tracked, validIdx0] = polyuPointTracker(Points0, lastFrame0, currentFrame0, polyuVO.minimumPoint, polyuVO.featurePara);
[Points1_tracked, validIdx1] = polyuPointTracker(Points1, lastFrame1, currentFrame1, polyuVO.minimumPoint, polyuVO.featurePara);
outliers0 = polyuRemoveLargeShift(Points0, Points0_tracked, validIdx0);
outliers1 = polyuRemoveLargeShift(Points1, Points1_tracked, validIdx1);
validIdx0(outliers0) = 0;
validIdx1(outliers1) = 0;
% filtering points
mp0_keyframe = polyuVO.keyframe{polyuVO.keyframeID}.fpt0(validIdx0,:);
mp1_keyframe = polyuVO.keyframe{polyuVO.keyframeID}.fpt1(validIdx1,:);
mp0_lastframe = Points0(validIdx0,:);
mp1_lastframe = Points1(validIdx1,:);
mp0_tracked = Points0_tracked(validIdx0,:);
mp1_tracked = Points1_tracked(validIdx1,:);
point3D0 = point3D0(validIdx0,:);
point3D1 = point3D1(validIdx1,:);
% tracking check
bad_distribution0 = polyuDistributionCheck(mp0_tracked, currentFrame0);
bad_distribution1 = polyuDistributionCheck(mp1_tracked, currentFrame1);
motion_flag0 = polyuMotionDetection(mp0_keyframe, mp0_tracked, polyuVO.keyframeMaximumMotion);
motion_flag1 = polyuMotionDetection(mp1_keyframe, mp1_tracked, polyuVO.keyframeMaximumMotion);
if size(mp0_tracked,1) < polyuVO.minimumPoint || ...
        size(mp1_tracked,1) < polyuVO.minimumPoint || ...
        bad_distribution0 || bad_distribution1 || motion_flag0 || motion_flag1
    disp('-----------------updating keyframe--------------------');
    [Points0, Points1, point3D0, point3D1] = polyuStereoView3dReconstruction(...
        lastFrame0, lastFrame1, cameraParams0, cameraParams1, polyuVO.T_c0c1, ...
        polyuVO.T_c1c0, polyuVO.featurePara, polyuVO.sos_para, polyuVO.minimumPoint);
    [Points0_tracked, validIdx0] = polyuPointTracker(Points0, lastFrame0, currentFrame0, polyuVO.minimumPoint, polyuVO.featurePara);
    [Points1_tracked, validIdx1] = polyuPointTracker(Points1, lastFrame1, currentFrame1, polyuVO.minimumPoint, polyuVO.featurePara);
    outliers0 = polyuRemoveLargeShift(Points0, Points0_tracked, validIdx0);
    outliers1 = polyuRemoveLargeShift(Points1, Points1_tracked, validIdx1);
    validIdx0(outliers0) = 0;
    validIdx1(outliers1) = 0;
    % filtering points
    mp0_keyframe = Points0(validIdx0,:);
    mp1_keyframe = Points1(validIdx1,:);
    mp0_lastframe = mp0_keyframe;
    mp1_lastframe = mp1_keyframe;
    mp0_tracked = Points0_tracked(validIdx0,:);
    mp1_tracked = Points1_tracked(validIdx1,:);
    point3D0 = point3D0(validIdx0,:);
    point3D1 = point3D1(validIdx1,:);
    % update keyframe pose
    polyuVO.keyframe{polyuVO.keyframeID}.pose = polyuVO.pose;
    UAV_pose_keyframe = polyuVO.keyframe{polyuVO.keyframeID}.pose;
    polyuVO.pre_T = eye(4);
end

% update data
polyuVO.tracks0 = mp0_tracked;
polyuVO.tracks1 = mp1_tracked;
polyuVO.lastFrame0 = currentFrame0;
polyuVO.lastFrame1 = currentFrame1;
polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam0 = point3D0;
polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam1 = point3D1;
polyuVO.keyframe{polyuVO.keyframeID}.fpt0 = mp0_keyframe;
polyuVO.keyframe{polyuVO.keyframeID}.fpt1 = mp1_keyframe;

% figure(2);
% subplot(1,2,1);
% showMatchedFeatures(lastFrame0, currentFrame0, mp0_lastframe, mp0_tracked);
% xlabel('cam 0');
% subplot(1,2,2);
% showMatchedFeatures(lastFrame1, currentFrame1, mp1_lastframe, mp1_tracked);
% xlabel('cam 1');
try
    [orient0, loc0, inlierIdx0] = estimateWorldCameraPose(mp0_tracked, point3D0, cameraParams0);
    [orient_r2f0, loc_r2f0] = polyuVisualServoing(mp0_tracked(inlierIdx0,:)', point3D0(inlierIdx0,:)', orient0, loc0, K0);
catch
    [orient0, loc, inlierIdx0] = ...
        polyuFindRelativePose(mp0_keyframe, mp0_tracked, cameraParams0, cameraParams1);
    loc0 = pre_loc_r2f0;
    [orient_r2f0, loc_r2f0] = polyuVisualServoing(mp0_tracked(inlierIdx0,:)', point3D0(inlierIdx0,:)', orient0, loc0, K0);
end
try
    [orient1, loc1, inlierIdx1] = estimateWorldCameraPose(mp1_tracked, point3D1, cameraParams1);
    [orient_r2f1, loc_r2f1] = polyuVisualServoing(mp1_tracked(inlierIdx1,:)', point3D1(inlierIdx1,:)', orient1, loc1, K1);
catch
    [orient1, loc, inlierIdx1] = ...
        polyuFindRelativePose(mp1_keyframe, mp1_tracked, cameraParams0, cameraParams1);
    loc1 = pre_loc_r2f1;
    [orient_r2f1, loc_r2f1] = polyuVisualServoing(mp1_tracked(inlierIdx1,:)', point3D1(inlierIdx1,:)', orient1, loc1, K1);
end
if polyuVO.xyOnly
    eul0 = rotm2eul(orient_r2f0);
    eul1 = rotm2eul(orient_r2f1);
    orient_r2f0 = eul2rotm([0 eul0(2) 0]);
    orient_r2f1 = eul2rotm([0 eul1(2) 0]);
end
estimate_T0 = [orient_r2f0',loc_r2f0'; 0 0 0 1];
estimate_T1 = [orient_r2f1',loc_r2f1'; 0 0 0 1];
T_uav0 = T_body2cam0 * estimate_T0 * T_cam2body0;
T_uav1 = T_body2cam1 * estimate_T1 * T_cam2body1;
confidence0 = sum(inlierIdx0)/length(inlierIdx0);
confidence1 = sum(inlierIdx1)/length(inlierIdx1);
T_uav = polyuPoseFusion(T_uav0, T_uav1, confidence0, confidence1);

% abrapt motion rejection
current_pose = UAV_pose_keyframe * T_uav;
current_ang = rotm2eul(current_pose(1:3,1:3));
current_ang = current_ang';
if size(polyuVO.motion_buff,2) > polyuVO.bufferSize
    mean_motion = mean(polyuVO.motion_buff, 2);
    if polyuVO.xyOnly
        last_rotation = polyuVO.motion_buff(1:3,end);
        current_rotate = max(abs(current_ang - last_rotation));
        if current_rotate > pi/6
            current_pose(1:3,1:3) = polyuVO.pose(1:3,1:3);
            disp('rotation rejected...');
        end
    end
    mean_position = mean_motion(4:6);
    current_shift = norm(current_pose(1:3,4) - mean_position);
    if current_shift > polyuVO.maxMotion
        current_pose(1:3,4) = polyuVO.pose(1:3,4);
        disp('translation rejected...');
    end
    polyuVO.motion_buff(:,1) = [];
end

polyuVO.pose = current_pose;
polyuVO.pre_T = T_uav;
polyuVO.motion_buff = [polyuVO.motion_buff, [current_ang;polyuVO.pose(1:3,4)]];

return