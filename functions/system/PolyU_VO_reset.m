function polyuVO = PolyU_VO_reset(polyuVO, img0, img1)
disp('-----------------reset frame--------------------');
polyuVO.lastFrame0 = img0;
polyuVO.lastFrame1 = img1;
polyuVO.stage = 0;
polyuVO.mapAvailable = 0;
cameraParams0 = polyuVO.cameraParams0;
cameraParams1 = polyuVO.cameraParams1;
feature_para = polyuVO.featurePara;
sos_para = polyuVO.sos_para;
disp('-----------------setting keyframe--------------------');
[mp0, mp1, point3D0, point3D1] = polyuStereoView3dReconstruction(...
    img0, img1, cameraParams0, cameraParams1, polyuVO.T_c0c1, ...
    polyuVO.T_c1c0, feature_para, sos_para, polyuVO.minimumPoint);
% update data
polyuVO.tracks0 = mp0;
polyuVO.tracks1 = mp1;
polyuVO.pre_T = eye(4);
polyuVO.motion_buff = [];
% initial keyframe
polyuVO.keyframe{polyuVO.keyframeID}.fpt0 = mp0;
polyuVO.keyframe{polyuVO.keyframeID}.fpt1 = mp1;
polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam0 = point3D0;
polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam1 = point3D1;
polyuVO.keyframe{polyuVO.keyframeID}.pose = polyuVO.pose;
return