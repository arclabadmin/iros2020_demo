clc;
clear all;
close all;
datapath = fullfile('.\data','results');
% our results
our = zeros(10,1);
% our_fps = [];
nametable = {'V1_01_easy','V1_02_medium','V1_03_difficult','V2_01_easy','V2_02_medium','MH_01_easy','MH_02_easy','MH_03_medium','MH_04_difficult','MH_05_difficult'}; % 'V2_03_difficult' is not available
result_date = '03-Sep-2019';
for iii = 1:5
    try
    filename = nametable{iii};
    filetype = ['_',result_date,'_vo_sos_result.mat'];
    results = importdata([datapath,'\',filename,filetype]);
    gt_t = results.gt_t;
    our_t = results.our_t;
    [R, t] = umeyama(our_t, gt_t);
    our_t_aligned = R * our_t + repmat(t,1,length(our_t));
    gt_t = gt_t';
    our_t_aligned = our_t_aligned';
    RMSE = sqrt(mean2((gt_t - our_t_aligned).^2));
    our(iii) = RMSE;
%     our_fps = [our_fps, results.fps];
    catch
    end
end

our_without_sos = zeros(10,1);

for iii = 1:5
    try
        filename = nametable{iii};
        filetype = ['_',result_date,'_vo_result.mat'];
        results = importdata([datapath,'\',filename,filetype]);
        gt_t = results.gt_t;
        our_t = results.our_t;
        [R, t] = umeyama(our_t, gt_t);
        our_t_aligned = R * our_t + repmat(t,1,length(our_t));
        gt_t = gt_t';
        our_t_aligned = our_t_aligned';
        RMSE = sqrt(mean2((gt_t - our_t_aligned).^2));
        our_without_sos(iii) = RMSE;
    catch
    end
end

Labeltable = {'V1 01 easy','V1 02 medium','V1 03 difficult','V2 01 easy','V2 02 medium'}; 
data = [our(1:5), our_without_sos(1:5)];
figure;
bar(data);
grid on;
legend('our', 'our (without sos refinement)');
ylabel('RMSE by meter');
% xlabel(['AVG FPS ', num2str(our_fps)]);
title('EuRoC MAV Dataset (Vicon Room)');
set(gca,'xticklabelmode','manual','xtick',1:5);
set(gca,'XTickLabel',Labeltable, 'Fontsize', 15);