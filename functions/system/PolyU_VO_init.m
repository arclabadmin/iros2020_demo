function polyuVO = PolyU_VO_init(img0, img1, feature_para, system_para,... 
    sos_para, cameraParams0, cameraParams1, T_BS_cam0, T_BS_cam1, UAV_pose)
% initial camera and UAV pose 
polyuVO.cameraParams0 = cameraParams0;
polyuVO.cameraParams1 = cameraParams1;
polyuVO.pose = UAV_pose;
polyuVO.lastFrame0 = img0;
polyuVO.lastFrame1 = img1;
% polyuVO.lastFrame0 = imsharpen(img0);
% polyuVO.lastFrame1 = imsharpen(img1);
% T_cam = pinv(T_uav2cam)
% T_cam * T_BS_cam = I and pinv(R) = R', so T_cam0 = [R', - R' * t; 0 0 0 1]
polyuVO.T_body2cam0 = T_BS_cam0;
polyuVO.T_body2cam1 = T_BS_cam1;
polyuVO.T_cam2body0 = polyuTransformInverse(T_BS_cam0);
polyuVO.T_cam2body1 = polyuTransformInverse(T_BS_cam1);
% transform from cam0 to cam1,  T_BS_cam0 * T_BS_c0c1 = T_BS_cam1
polyuVO.T_c0c1 = polyuTransformInverse(T_BS_cam0) * T_BS_cam1;
% transform from cam1 to cam0
polyuVO.T_c1c0 = polyuTransformInverse(polyuVO.T_c0c1);
% load parameters
polyuVO.featurePara = feature_para;
polyuVO.keyframeMaximumMotion = system_para(1);
polyuVO.minimumPoint = system_para(2);
polyuVO.bufferSize = system_para(3);
polyuVO.maxMotion = system_para(4);
polyuVO.xyOnly = system_para(5);
polyuVO.sos_para = sos_para;
% initial VO system
polyuVO.keyframeID = 1; % reserve for future work
polyuVO.scaleFactor = 1; % scale the estimated loc to real distance
polyuVO.stage = 0;
polyuVO.mapAvailable = 0;
% initial keyframe
disp('-----------------setting keyframe--------------------');
[mp0, mp1, point3D0, point3D1] = polyuStereoView3dReconstruction(...
    img0, img1, cameraParams0, cameraParams1, polyuVO.T_c0c1, ...
    polyuVO.T_c1c0, feature_para, sos_para, polyuVO.minimumPoint);
% update data
polyuVO.tracks0 = mp0;
polyuVO.tracks1 = mp1;
polyuVO.pre_T = eye(4);
polyuVO.motion_buff = [];
% initial keyframe
polyuVO.keyframe{polyuVO.keyframeID}.fpt0 = mp0;
polyuVO.keyframe{polyuVO.keyframeID}.fpt1 = mp1;
polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam0 = point3D0;
polyuVO.keyframe{polyuVO.keyframeID}.X3D_cam1 = point3D1;
polyuVO.keyframe{polyuVO.keyframeID}.pose = polyuVO.pose;

return

% rotationOfCamera2 = polyuVO.T_c0c1(1:3,1:3);
% translationOfCamera2 = polyuVO.T_c0c1(1:3,4);
% polyuVO.stereoParams = stereoParameters(cameraParams0,cameraParams1,rotationOfCamera2,translationOfCamera2);
% [frameLeftRect, frameRightRect] = rectifyStereoImages(img0, img1, polyuVO.stereoParams);
% figure;
% imshow(stereoAnaglyph(frameLeftRect, frameRightRect));
% title('Rectified Video Frames');
% disparityMap = disparity(frameLeftRect, frameRightRect);
% figure;
% imshow(disparityMap, [0, 64]);
% title('Disparity Map');
% colormap jet
% colorbar
% points3D = reconstructScene(disparityMap, polyuVO.stereoParams);
% % Convert to meters and create a pointCloud object
% points3D = points3D ./ 1000;
% ptCloud = pointCloud(points3D);
% % Create a streaming point cloud viewer
% player3D = pcplayer([-10, 10], [-10, 10], [-10, 10], 'VerticalAxis', 'y', ...
%     'VerticalAxisDir', 'down');
% % Visualize the point cloud
% view(player3D, ptCloud);