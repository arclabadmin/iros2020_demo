function SOS_infeasible_index = polyuLMIsFeasibleCheckMOSEK(point_X, point_x, lb, ub, point_number, check_index)

if nargin < 5
    point_number = size(point_x,2);
    check_index = ones(point_number,1);
elseif nargin < 6
    check_index = ones(point_number,1);
end

SOS_infeasible_index = check_index;
pair_i = 1;
point_X(4,:) = 1;

while pair_i <= point_number
    
    if check_index(pair_i) == 1
    
        % get Qi for polynomials
        p1 = point_X(:,pair_i);
        p2 = point_x(:,pair_i);
        p1 = p1/p1(3);
        p2 = p2/p2(3);
        Qf = compute_Q(p1(1),p1(2),p1(3),p2(1),p2(2));

        % get bound
        varMinMax = [lb, ub];

        Qg = getQg(varMinMax);
        n_Qg = size(Qg,3);

        cvx_begin quiet
        variable t(3)
        variable s(n_Qg) nonnegative
        minimize(bsxfun(@times, Qf, reshape(t,1,1,[])) - bsxfun(@times, Qg, reshape(s,1,1,[])));
        cvx_end
        
        if cvx_status == 'Infeasible'
            SOS_infeasible_index(pair_i) = 0;
        end
    end
    
    pair_i = pair_i + 1;
    
end

end