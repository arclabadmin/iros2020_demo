function inlier_refined = polyuSumOfSquareRefinement(point2D, points3D, orientation, location, cameraParams, sos_para)

K = cameraParams.IntrinsicMatrix';
[ub, lb] = polyuGetBoundary(orientation, location, sos_para(1), sos_para(2));
point2D(3,:) = 1;
point2D = pinv(K) * point2D;
point2D = point2D./repmat(point2D(3,:),3,1);
SOS_feasible_index = polyuLMIsFeasibleCheck(points3D, point2D, lb, ub);
inlier_refined = find(SOS_feasible_index == 1);

return