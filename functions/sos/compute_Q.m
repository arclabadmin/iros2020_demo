function Q = compute_Q(Xx,Xy,Xz,xx,xy)

Q = [];
Q1 = compute_Q1(Xx,Xy,Xz,xx,xy);
Q2 = compute_Q2(Xx,Xy,Xz,xx,xy);
Q3 = compute_Q3(Xx,Xy,Xz,xx,xy);
Q(:,:,1) = Q1;
Q(:,:,2) = Q2;
Q(:,:,3) = Q3;

% q1_1 = Xy  - Xz*xy;
% q1_2 = (Xy - Xx + Xz*xx - Xz*xy)/2;
% q1_3 = (Xy + Xx*xy - Xy*xx - Xz*xy)/2;
% q1_4 = (- xy - 1)/2;
% q1_5 = xy/2;
% q1_6 = 1/2;
% q1_7 = (2*Xz - Xx - Xx*xy - Xy*xx + 2*Xy*xy - Xz*xx)/2;
% q2_2 = Xz*xx - Xx;
% q2_3 = (Xx*xy - Xx - Xy*xx + Xz*xx)/2;
% q2_4 = xx/2;
% q2_5 = (- xx - 1)/2;
% q2_6 = 1/2;
% q2_7 = (2*Xz - Xy + 2*Xx*xx - Xx*xy - Xy*xx - Xz*xy)/2;
% q3_3 = Xx*xy - Xy*xx;
% q3_4 = xx/2;
% q3_5 = xy/2;
% q3_6 = (- xx - xy)/2;
% q3_7 = (2*Xx*xx - Xy - Xx + 2*Xy*xy - Xz*xx - Xz*xy)/2;
% q4_4 = 0;
% q4_5 = 0;
% q4_6 = 0;
% q4_7 = (1 - xy)/2;
% q5_5 = 0;
% q5_6 = 0;
% q5_7 = (xx - 1)/2;
% q6_6 = 0;
% q6_7 = (xy - xx)/2;
% q7_7 = Xx - Xy - Xx*xy + Xy*xx - Xz*xx + Xz*xy;
% 
% Q = [q1_1, q1_2, q1_3, q1_4, q1_5, q1_6, q1_7;
%     q1_2, q2_2, q2_3, q2_4, q2_5, q2_6, q2_7;
%     q1_3, q2_3, q3_3, q3_4, q3_5, q3_6, q3_7;
%     q1_4, q2_4, q3_4, q4_4, q4_5, q4_6, q4_7;
%     q1_5, q2_5, q3_5, q4_5, q5_5, q5_6, q5_7;
%     q1_6, q2_6, q3_6, q4_6, q5_6, q6_6, q6_7;
%     q1_7, q2_7, q3_7, q4_7, q5_7, q6_7, q7_7];

end