function [ub, lb] = polyuGetBoundary(pre_orient, pre_loc, r_var, t_var)
    x_var = ones(6,1);
    x_var(1:3) = r_var .* x_var(1:3);
    x_var(4:6) = t_var .* x_var(4:6);
    v_pre = cayley_R2v(pre_orient);
    t_pre = (eye(3) - skew(v_pre)) * (-pre_loc*pre_orient')';
    x = [v_pre; t_pre];
    ub = x + x_var;
    lb = x - x_var;
return