%% This is the KITTI dataset test demo for IROS2020 paper:
% Accurate Stereo Visual Odometry for UAV using Sum-of-Square-based Orientation Prior
% Contact: Ran Duan (ran-sn.duan@connect.polyu.hk, rduan036@gmail.com)
% Adaptive Robotic Controls Lab, Hong Kong Polytechnic University

% please download the the following data into ./kitti
% odometry data set (grayscale, 22 GB)
% odometry data set (calibration files, 1 MB)

% download link:
% http://www.cvlibs.net/datasets/kitti/eval_odometry.php

% location of sequences and calibration files: .\kitti\dataset\sequences

% the groundtruth of sequence 11-15 is given in the demo files, you can 
% also download them from the same link (they are extracted from other 
% algorithms' results).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all;
addpath(genpath('./functions'));
addpath(genpath('./mexopencv'));
%% load data of KITTI
sequence_id = '11'; % change the sequence id for your test
left_view_dir = fullfile('.\kitti\dataset\sequences',sequence_id,'image_0');
right_view_dir = fullfile('.\kitti\dataset\sequences',sequence_id,'image_1');
calib_path = fullfile('.\kitti\dataset\sequences',sequence_id,'calib.txt');
groundtruth_path = fullfile('.\kitti\groundtruth',[sequence_id,'.txt']);
gt_t = importdata(groundtruth_path);
gt_t(:,3:4) = [];

img0_list = dir(fullfile(left_view_dir,'*.png'));
img1_list = dir(fullfile(right_view_dir,'*.png'));
img0 = imread(fullfile(left_view_dir,img0_list(1).name));
img1 = imread(fullfile(right_view_dir,img1_list(1).name));

%% Camera Info
% calibration parameters for sequence 2010_03_09_drive_0000
imageSize = size(img0);
calib_data = dlmread(calib_path,' ',0,1);
P0 = reshape(calib_data(1,:),[4,3])';
P1 = reshape(calib_data(2,:),[4,3])';
Tr_velo_to_cam = reshape(calib_data(5,:),[4,3])'; % Tr data in calib files
% P = [KR,Kt]
% http://www.cvlibs.net/datasets/kitti/setup.php
% cam0 to cam1: R = I, t = 0.54(m)
% K_cam0 = K_cam1 = K
K = P0(:,1:3);
cameraParams0 = cameraParameters('IntrinsicMatrix',K', 'ImageSize', imageSize);
cameraParams1 = cameraParameters('IntrinsicMatrix',K', 'ImageSize', imageSize);
%% VO paras
% MinQuality, grid number [H, W], number of feature in each grid, invalid
% edge(pixcel), orb feature enable
feature_para = [0.02, 1, 2, 320, 5, 1];
% keyframe maximum motion, minimum tracked points, trace filter buffer, 
% maximum movement(in meter), 2D map VO flag
system_para = [40, 20, 5, 8, 1];
% SOS rotation interval, translation interval, enable flag
sos_para = [0.0001, 1, 1];
% body frame to camera frame
T_BS_cam0 = polyuTransformInverse([Tr_velo_to_cam; 0 0 0 1]);
R_cam1 = eye(3);
t_cam1 = pinv(K) * (P0(:,4) - P1(:,4));
T_c0c1 = [R_cam1,t_cam1; 0 0 0 1];
T_BS_cam1 = T_BS_cam0 * T_c0c1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init VO
% UAV_pose = groundtruth_pose{1};
init_pose = eye(4);
our_traces = [0,0];
time = 0;
tic;
polyuVO = PolyU_VO_init(img0, img1, feature_para, system_para,...
    sos_para, cameraParams0, cameraParams1, T_BS_cam0, T_BS_cam1, init_pose);
time = time + toc;
frame_counter = 1;
total_frame = length(img0_list);
while frame_counter < total_frame
    frame_counter = frame_counter + 1;
    img0 = imread(fullfile(left_view_dir,img0_list(frame_counter).name));
    img1 = imread(fullfile(right_view_dir,img1_list(frame_counter).name));
    tic;
    polyuVO = PolyU_VO_step(polyuVO, img0, img1);
    time = time + toc;
    fps = frame_counter/time
    cam_position = polyuVO.pose(1:3,4);
    car_body_position = [-cam_position(2),cam_position(1)];
    our_traces = [our_traces; car_body_position];
    figure(1);
    clf;
    subplot(2,1,1);
    hold on;
    plot(gt_t(:,1), gt_t(:,2),'g','LineWidth',3);
    plot(our_traces(:,1), our_traces(:,2), 'k' ,'LineWidth',2);
    title([' Frame: ' num2str(frame_counter)] );
    hold off;
    xlabel('x(m)');
    ylabel('y(m)');
    legend('Groundtruth','Our trace','Location', 'northwest');
    % show keypoints
    points_loc0 = polyuVO.tracks0;
    points_loc1 = polyuVO.tracks1;
    points_loc1(:,1) = points_loc1(:,1) + size(img0,2)*ones(size(points_loc1(:,1)));
    keypoints = [points_loc0; points_loc1];
    image_views = [img0, img1];
    subplot(2,1,2);
    imshow(image_views);
    hold on;
    plot(keypoints(:,1), keypoints(:,2), '.g');
    hold off;
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.2, 0.1, 0.4, 0.8]);
    drawnow;
end
% result analysis
gt_t(:,3) = zeros(length(gt_t),1);
our_t(:,1) = downsample(our_traces(:,1),3);
our_t(:,2) = downsample(our_traces(:,2),3);
our_t(:,3) = zeros(length(gt_t),1);
[R, t] = umeyama(our_t', gt_t');
our_t_aligned = R * our_t' + repmat(t,1,length(our_t'));
our_t_aligned = our_t_aligned';
RMSE_our = sqrt(mean2((gt_t - our_t_aligned).^2));
figure;
hold on;
plot(gt_t(:,1),gt_t(:,2),'g','LineWidth',3);
plot(our_t_aligned(:,1),our_t_aligned(:,2),'r','LineWidth',2);
hold off;
legend('Groundtruth',['Our (',num2str(RMSE_our),')'],'Location', 'northwest');
xlabel('x(m)');
ylabel('y(m)');
title(['RMSE of Sequence ',sequence_id]);
grid on;